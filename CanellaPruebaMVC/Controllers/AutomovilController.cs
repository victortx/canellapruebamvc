﻿using CanellaPruebaMVC.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CanellaPruebaMVC.Controllers
{
    public class AutomovilController : Controller
    {
        private string UrlApi = "https://localhost:44303/api";
        // GET: Automovil
        public async Task<ActionResult> Index()
        {
            // servicio para obtener la lista de autos
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync($"{UrlApi}/Automovil");
            List<Automovil> response = JsonConvert.DeserializeObject<List<Automovil>>(json);
            return View(response);
        }

        // GET: Automovil/Details/5
        public async Task<ActionResult> Details(int id)
        {
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync($"{UrlApi}/Automovil/{id}");
            Automovil response = JsonConvert.DeserializeObject<Automovil>(json);
            return View(response);
        }

        // GET: Automovil/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Automovil/Create
        [HttpPost]
        public async Task<ActionResult> Create(Automovil Auto)
        {
            try
            {
                // seteo de datos para el post
                var json = JsonConvert.SerializeObject(Auto);
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var httpClient = new HttpClient();
                var response = await httpClient.PostAsync($"{UrlApi}/Automovil", data);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Automovil/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync($"{UrlApi}/Automovil/{id}");
            Automovil response = JsonConvert.DeserializeObject<Automovil>(json);
            return View(response);
        }

        // POST: Automovil/Edit/5
        [HttpPost]
        public  async Task<ActionResult> Edit(int id, Automovil Auto)
        {
            try
            {
                var json = JsonConvert.SerializeObject(Auto);
                var data = new StringContent(json, Encoding.UTF8, "application/json");
                var httpClient = new HttpClient();
                var response = await httpClient.PutAsync($"{UrlApi}/Automovil/{id}", data);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Automovil/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync($"{UrlApi}/Automovil/{id}");
            Automovil response = JsonConvert.DeserializeObject<Automovil>(json);
            return View(response);
        }

        // POST: Automovil/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Automovil collection)
        {
            try
            {
                var httpClient = new HttpClient();
                var json = await httpClient.DeleteAsync($"{UrlApi}/Automovil/{id}");
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
