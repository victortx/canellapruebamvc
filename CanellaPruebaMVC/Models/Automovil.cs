﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CanellaPruebaMVC.Models
{
    public class Automovil
    {
        public int Id { get; set; }

        public string Matricula { get; set; }
        public string Marca { get; set; }
        public string Color { get; set; }
        public decimal Precio { get; set; }

    }
}